//
//  SimpleTableViewController.h
//  CustomTableView
//
//  Created by Rifat Firdaus on 6/18/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@end
