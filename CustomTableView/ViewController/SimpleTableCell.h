//
//  SimpleTableCell.h
//  CustomTableView
//
//  Created by Rifat Firdaus on 6/18/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *prepTimeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;

@end
