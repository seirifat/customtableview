//
//  SimpleTableCell.m
//  CustomTableView
//
//  Created by Rifat Firdaus on 6/18/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import "SimpleTableCell.h"

@implementation SimpleTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
